# Generated by Django 3.0.6 on 2020-05-16 09:26

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContractTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300)),
                ('description', models.TextField()),
                ('html', models.TextField()),
                ('create_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='ContractTemplateParameters',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('place_holder', models.CharField(max_length=200)),
                ('label', models.TextField()),
                ('contract_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contracts.ContractTemplate')),
            ],
        ),
        migrations.AddConstraint(
            model_name='contracttemplateparameters',
            constraint=models.UniqueConstraint(fields=('contract_id', 'place_holder'), name='unique_contract_template_parameters'),
        ),
    ]
