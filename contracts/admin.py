from django.contrib import admin
from .models import ContractTemplate, ContractTemplateParameters

# Register your models here.
admin.site.register(ContractTemplateParameters)
admin.site.register(ContractTemplate)
