from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('contracts', views.contract, name='contracts'),
    path('impressum', views.impressum, name='impressum'),
    path('policy', views.policy, name='policy'),
    path('contract/<int:contract_id>/', views.detail, name='detail'),
]
