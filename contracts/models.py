from django.db import models
from django.utils import timezone
from tinymce.models import HTMLField


# Create your models here.
class ContractTemplate(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField()
    # html = models.TextField()  # HTMLField() #models.TextField()
    html = HTMLField()
    create_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title


class ContractTemplateParameters(models.Model):
    contract_id = models.ForeignKey(ContractTemplate, on_delete=models.CASCADE)
    place_holder = models.CharField(max_length=200)
    label = models.TextField()

    def __str__(self):
        return self.place_holder

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['contract_id', 'place_holder'], name='unique_contract_template_parameters'),
        ]