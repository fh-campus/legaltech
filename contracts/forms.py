from django import forms
from tinymce.widgets import TinyMCE

class DynamicForm(forms.Form):
    def __init__(self, *args, **kwargs):
        ugly_params = kwargs.pop('my_params')
        super(DynamicForm, self).__init__(*args, **kwargs)
        for item in ugly_params:  # range(5): # .replace(" ", "_")
            if "|large" in item:
                self.fields[item] = forms.CharField(widget=TinyMCE(attrs={'cols': 50, 'rows': 5}))
            else:
                self.fields[item] = forms.CharField(max_length=255, label=item)