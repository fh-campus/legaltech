import re

from django.shortcuts import render
from xhtml2pdf import pisa
from .forms import DynamicForm
from .models import ContractTemplate
from django.http import Http404

# Create your views here.
from django.http import HttpResponse
import html

def index(request):
    return render(request, 'contracts/../contracts/hallo.html', )

def contract(request):
    contracts = ContractTemplate.objects.order_by('create_date')
    return render(request, 'contracts/../contracts/list.html', {'contracts': contracts})


def detail(request, contract_id):
    try:
        contract = ContractTemplate.objects.get(pk=contract_id)
        # contract.html = re.compile(r"\{([^\}]*)\}").sub(r"#####\1", contract.html)
        #contract.html = re.compile(r"\{([^\}]*)\}").sub(r"#####\1", contract.html)

        contract.html = re.compile(r"\{([^\}\{]*)(\{([^\}\{]*)\}){2,}\}").sub(r"[AUSWAHLFELD]", contract.html)  # nur eines!?

        # Einzelne Optionen (Checkboxes) Ein-/Ausschalten
        contract.html = re.compile(r"\{([^\}\{]*)(\{([^\}\{]*)\})+\}").sub(r"<div><input type='checkbox' onchange='toggleOption(event.target)' /><span class='optHead'>&nbsp;\1</span><div class='optional'>\3</div></div>", contract.html) # nur eines!?
        #contract.html = re.compile(r"\{([^\}\{]*)(\{([^\}\{]*)\})+\}").sub(r"[OPTINALES FELD KOMMT HIER]", contract.html)

        #options = re.findall(r"\{([^\}\{]*)(\{([^\}\{]*)\})+\}", contract.html)

        # Test um CSS zu ändern für PDF-Druck
        contract.html = "<style> /* tob dich aus */ </style>" + contract.html  #.replace('<h1>','<h1 style="" class="blub test">')
        tmp = re.findall(r"\[(.*?)\]", contract.html)
        tmp = list(dict.fromkeys(tmp))  # dictonary statt liste (und implizit "gleiche Parameter verschmelzen")
        params = []
        for t in tmp:
            p = html.unescape(t)
            params.append(p)
        params_form = DynamicForm(request.POST or None, my_params=params)

        # feedback = {}
        contract.filled_contract = contract.html

        if request.method == 'POST':

            for key in params_form.fields:
                contract.filled_contract = contract.filled_contract.replace('[' + key + ']', params_form.data[key])

            if 'print' in request.POST:
                try:
                    resp = HttpResponse(content_type='application/pdf')
                    pisa.CreatePDF(contract.filled_contract.encode("UTF-8"), resp, encoding='UTF-8',
                                   link_callback=None)
                    return resp
                except:
                    raise Http404("Error creating PDF!")

            # Have fun with parameters here
            pass

            # #form = ParamsForm(request.POST)
            # #if params_form.is_valid():
            # if not paramForm.is_valid():
            #     feedback = {'not valid':'not valid'}
            # else:
            #     feedback = params_form.cleaned_data
            #     pass
            #     # for p in form.cleaned_data:
            #     #     feedback[p.get] = request.POST.get(p, '')

    except ContractTemplate.DoesNotExist:
        raise Http404("Contract does not exist")
    return render(request, 'contracts/detail.html',
                  {'contract': contract, 'params_form': params_form, })  # 'paramListFeedback': feedback})


def impressum(request):
    return render(request, 'contracts/impressum.html')


def policy(request):
    return render(request, 'contracts/policy.html')
